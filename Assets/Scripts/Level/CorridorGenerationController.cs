﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CorridorGenerationController : MonoBehaviour
{
    [SerializeField]
    private Corridor _Corridor = null;

    private List<Corridor> _Corridors = new List<Corridor>();

    private int _ObstacleSpawned = 0;

    private void Start()
    {
        for (int i = 0; i < 10; i++)
        {
            GameObject go = Instantiate(_Corridor.gameObject, new Vector3(0, 0, i * 5), Quaternion.identity);
            Corridor cor = go.GetComponent<Corridor>();
            _Corridors.Add(cor);
            cor.Init(false);
        }
    }

    public void UpdateCGC()
    {
        float moveOffset = 5f * GameController.Get().DeltaTime;
        for (int i = 0; i < _Corridors.Count; i++)
        {
            Transform trans = _Corridors[i].transform;
            Vector3 currentPosition = trans.position;
            currentPosition.z -= moveOffset;
            trans.position = currentPosition;
        }

        //podminka jestli je prvni plocha za hracem
        //pokud je plocha za hracem vyhodi se prvni plocha ze seznamu,
        //nastavi se ji pozice podle posledni plochy v seznamu a prida se zpatky do seznamu

        Corridor corridor = _Corridors[0];
        if (corridor.transform.position.z < -7f)
        {
            _ObstacleSpawned++;
            _Corridors.RemoveAt(0);

            int lastIndex = _Corridors.Count - 1;
            Vector3 lastPosition = _Corridors[lastIndex].transform.position;
            lastPosition.z += 5f;
            corridor.transform.position = lastPosition;

            corridor.Init(_ObstacleSpawned % 2 == 0);

            _Corridors.Add(corridor);
        }
    }
}
