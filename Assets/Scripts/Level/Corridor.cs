﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Corridor : MonoBehaviour
{
    //ma promennou ve ktere jsou ulozeny 3 pozice kde se muze spawnovat prekazka
    //ma mit metodu ktera nahodne (Random) vybere pozici a vytvori novej gameobject treba kostku a vlozi ji na pozici
    //ale musi ji vlozit tak aby ta kostka byla zaroven potomek toho corridoru
    // Instantiate(kostka, pozice, rotaci, transform); 
    // GameObject go = Instantiate(kostka, pozice, rotace); go.transform.SetParent(transform);

    [SerializeField] private Transform[] _ObstaclePlaces = null;

    private GameObject _Obstacle = null;

    public void Init(bool pSpawnObstacle)
    {
        if (pSpawnObstacle)
        {
            CreateObstacle();
        }
    }

    private void CreateObstacle()
    {
        if (_Obstacle != null)
        {
            Destroy(_Obstacle);
        }

        int index = Random.Range(0, _ObstaclePlaces.Length);
        Vector3 obstaclePosition = _ObstaclePlaces[index].position;

        _Obstacle = GameController.Get().ObstacleGenerationController.CreateObstacle
        (
            obstaclePosition,
            Quaternion.identity, 
            transform
        );
    }
}
