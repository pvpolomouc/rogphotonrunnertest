﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleGenerationController : MonoBehaviour
{
    [SerializeField] private GameObject _ObstaclePrefab = null;

    public GameObject CreateObstacle(Vector3 pPosition, Quaternion pRotation, Transform pParent)
    {
        GameObject go = Instantiate(_ObstaclePrefab, pPosition, pRotation, pParent);

        return go;
    }
}
