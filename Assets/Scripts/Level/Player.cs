﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] private CharacterController    _CharacterController = null;
    [SerializeField] private float                  _JumpHeight = 4f;
    [SerializeField] private float                  _Gravity = 9.81f;
    [SerializeField] private float                  _Speed = 10f;

    private int     _LineIndex = 1;
    private Vector3 _Move = Vector3.zero;
    private bool    _IsGrounded = false;
    private int     _FrameCount = 0;

    [SerializeField]
    private float[] _PositionsX = new float[]
    {
        -2, //0
        0, //1
        2, //2
    };

    public void UpdatePlayer()
    {
        _FrameCount++;
        _IsGrounded = _CharacterController.isGrounded;
        Vector3 currentPosition = transform.position;
        Vector3 targetPosition = currentPosition;
        targetPosition.x = 0;

        if (Input.GetKeyDown(KeyCode.A))
        {
            _LineIndex--;
        }
        else if (Input.GetKeyDown(KeyCode.D))
        {
            _LineIndex++;
        }

        _LineIndex = Mathf.Clamp(_LineIndex, 0, 2);
        targetPosition.x = _PositionsX[_LineIndex];
        _Move.x = (targetPosition - currentPosition).x * _Speed;

        if (!_CharacterController.isGrounded && _FrameCount % 2 == 0)
        {
            RaycastHit hit;
            if (Physics.SphereCast(currentPosition, 0.5f, Vector3.down, out hit, 1.2f))
            {
                _IsGrounded = true;
            }
        }

        if (Input.GetKeyDown(KeyCode.Space) && _IsGrounded)
        {
            _Move.y = _JumpHeight;
            Debug.Log(_Move.y);
        }

        if (!_CharacterController.isGrounded)
        {
            _Move.y -= _Gravity * GameController.Get().DeltaTime;
        }

        _CharacterController.Move(_Move * GameController.Get().DeltaTime);
    }
}