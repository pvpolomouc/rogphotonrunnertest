﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreCount : MonoBehaviour
{
    public Text scoreText;
    public Text hiScoreText;

    public float scoreCount;
    public float hiScoreCount;

    public float pointPerSecond;

    public bool scoreIncreasing;
        
    void Start()
    {
        
    }

    void Update()
    {
        scoreCount += pointPerSecond + Time.deltaTime;

        if(scoreCount > hiScoreCount)
        {
            hiScoreCount = scoreCount;
        }

        scoreText.text = "SCORE: " + Mathf.Round (scoreCount);
        hiScoreText.text = "HISCORE: " + Mathf.Round (hiScoreCount);
    }
}
