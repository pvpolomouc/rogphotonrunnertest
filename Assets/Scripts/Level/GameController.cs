﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    private static GameController _Instance = null;

    public static GameController Get()
    {
        return _Instance;
    }

    [SerializeField] private Player                         _Player = null;
    [SerializeField] private CorridorGenerationController   _CorridorGenerationController = null;
    [SerializeField] private ObstacleGenerationController   _ObstacleGenerationController = null;
    [SerializeField] private float   _GameSpeed = 1f;

    private float _DeltaTime = 0f;

    public float DeltaTime
    {
        get { return _DeltaTime; }
    }

    public ObstacleGenerationController ObstacleGenerationController
    {
        get
        {
            return _ObstacleGenerationController;
        }
    }

    private void Awake()
    {
        if (_Instance == null)
        {
            _Instance = this;
        }
        else
        {
            Debug.LogError("Vytvoril se dalsi GameController!!!");
        }

        ObstacleGenerationController ogc = ObstacleGenerationController;
    }

    private void Update()
    {
        _DeltaTime = Time.deltaTime * _GameSpeed;
        _Player.UpdatePlayer();
        _CorridorGenerationController.UpdateCGC();
    }
}
